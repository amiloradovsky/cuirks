
#include <stdlib.h>
#include <stdio.h>
#include <sysexits.h>

#include "carry.h"

int main (int argc, char *argv[])
{
  u_int16_t x = atoi (argv[1]);
  u_int16_t y = atoi (argv[2]);

  u_int16_t n = ~(~0 << 8);  /* mask */
  u_int16_t h = (x >> 8) * (y >> 8);  /* high */
  u_int16_t m0 = (x >> 8) * (y & n);
  u_int16_t m1 = (x & n) * (y >> 8);
  u_int16_t m = m0 + m1;  /* middle/medium */
  u_int16_t l = (x & n) * (y & n);  /* low */
  u_int16_t h_ = h + (m >> 8)
    + (carry (16, m0, m1) << 8)
    + carry (16, l, (m << 8));
  u_int16_t l_ = x * y;  /* l + (m << 8) */
  u_int32_t z = (h_ << 16) | (l_ << 0);
  /* __uint128_t t = 0; */

  printf ("mask = %x; %x, %x, %x\n", n, h, m, l);
  printf ("%x, %x: z = %x\n", h_, l_, z);
  printf ("x = %u, y = %u, x * y = %u (%x)\n", x, y, x * y, x * y);

  return EX_OK;
}

/*
  gcc -Wall multi.c
  ./a.out 32767 32767
  ./a.out 32768 32767
  ./a.out 32767 32768
  ./a.out 32768 32768
  ./a.out 0 0
  ./a.out 0 1
  ./a.out 1 0
  ./a.out 1 1
  ./a.out -1 1
  ./a.out 1 -1
  ./a.out -1 -1
*/
