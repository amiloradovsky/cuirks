
#include <stdint.h>

#define carry(width, x, y)                                      \
  ((u_int ## width ## _t) x > (u_int ## width ## _t) ~y)
