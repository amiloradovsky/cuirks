
#include <stdlib.h>
#include <stdio.h>
#include <sysexits.h>

#include "carry.h"

int main (int argc, char *argv[])
{
  u_int8_t  u8x  = atoll (argv[1]);
  u_int8_t  u8y  = atoll (argv[2]);
  u_int16_t u16x = atoll (argv[1]);
  u_int16_t u16y = atoll (argv[2]);
  u_int32_t u32x = atoll (argv[1]);
  u_int32_t u32y = atoll (argv[2]);
  u_int64_t u64x = atoll (argv[1]);
  u_int64_t u64y = atoll (argv[2]);

  printf ("%d, %d, %d, %d\n",
          carry (8,  u8x,  u8y),  carry (16, u16x, u16y),
          carry (32, u32x, u32y), carry (64, u64x, u64y));

  printf ("%hhx + %hhx = %hhx + %hhx (8)\n",
          u8x, u8y, u8x + u8y, carry (8, u8x, u8y));
  printf ("%hx + %hx = %hx + %hx (16)\n",
          u16x, u16y, u16x + u16y, carry (16, u16x, u16y));
  printf ("%x + %x = %x + %x (32)\n",
          u32x, u32y, u32x + u32y, carry (32, u32x, u32y));
  printf ("%lx + %lx = %lx + %x (64)\n",
          u64x, u64y, u64x + u64y, carry (64, u64x, u64y));

  return EX_OK;
}

/*
  gcc -Wall carry.c
  ./a.out 0 0
  ./a.out 0 -1
  ./a.out -1 0
  ./a.out -1 -1
  ./a.out 127 127
  ./a.out 128 127
  ./a.out 127 128
  ./a.out 128 128
  ./a.out 32767 32767
  ./a.out 32768 32767
  ./a.out 32767 32768
  ./a.out 32768 32768
*/
